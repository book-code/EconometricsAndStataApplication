// 函数normalden(x)与normal(x)分别表示正态分布的概率密度函数与累积分布函数
// 计算标准正态变量小于1.96的概率
dis normal(1.96)
// 画标准正态的密度函数，range表示横轴范围，xline(0)表示在x=0处画一条直线
twoway function y = normalden(x),range(-5 5) xline(0) ytitle(概率密度)
// 将N(0,1)与N(1,4)的密度函数画在一起
twoway function y = normalden(x),range(-5 10) || function z = normalden(x,1,2),range(-5 10) lpattern(dash) ytitle(概率密度)

// 函数chi2den(k,x)与chi2(k,x)分别表示自由度为k的卡方分布的概率密度函数与累积分布函数
// 将自由度为3和5的卡方分布的概率密度函数画在一起
twoway function chi3 = chi2den(3,x),range(0 20) || function chi5 = chi2den(5,x),range(0 20) lpattern(dash) ytitle(概率密度)

// 函数tden(k,t)与t(k,t)分别表示自由度为k的t分布的概率密度函数与累积分布函数
// 将t(1)与t(5)的概率密度函数画在一起，lpattern(dash)表示画虚线
twoway function t1 = tden(1,x),range(-5 5) || function t5 = tden(5,x),range(-5 5) lpattern(dash) ytitle(概率密度)

// 函数Fden(k1,k2,x)与F(k1,k2,x)分别表示自由度为(k1,k2)的F分布的概率密度函数与累积分布函数
// 将F(10,20)与F(10,5)的概率密度函数画在一起
twoway function F20 = Fden(10,20,x),range(0 5) || function F5 = Fden(10,5,x),range(0 5) lpattern(dash) ytitle(概率密度)