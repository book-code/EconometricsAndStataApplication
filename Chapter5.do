* 5.1 二元线性回归
use cobb_douglas.dta,clear
// 查看数据集中的观测值
list
// 进行二元回归，lnk资本对数，lnl劳动力对数
reg lny lnk lnl
// 做完OLS回归后，可用命令predict来计算拟合值与残差
// 将lny的拟合值记为lny1
predict lny1
// 将lny的残差记为e
predict e,r
// 将lny及其拟合值、残差同时列出
list lny lny1 e
// 将产出对数及其拟合值画在一起
line lny lny1 year,lpattern(solid dash)

* 5.12 多元回归的Stata实例
use grilic.dta,clear
// s(教育年限) expr(工龄) tenure(在现在单位工作年限) smsa(是否住在大城市) rns(是否住在美国南方)
reg lnw s expr tenure smsa rns
/**
 * 整个方程的F统计量为81.75，对应的p值(Prob>F)为0，表明这个回归方程整体是高度显著的。
 * 所有解释变量的回归系数的p值(P>|t|)都小于0.01，故均在1%水平上显著，而且符号与理论预期一致。
 * 教育年限(s)的系数估计值为0.103，即教育投资回报率为10.3%。
 * expr与tenure的回报率分别为3.8%与3.6%，小于正规教育的回报率。
 * smsa的回报率高达14%，甚至高于一年教育的回报率，说明了环境的重要性。
 * rns的系数为-0.084，表明南方居民的工资比北方居民低8.4%。
 * 常数项的估计值为4.104，表明未受任何教育(s=0)、也无工作经验(expr=tenure=0)、不住在大城市(smsa=0)，且住在南方(rns=0)的人预期工资对数为4.104
 */

// 显示回归系数的协方差矩阵：主对角线元素为回归系数的方差，非主对角线元素为协方差
vce
// 为了演示目的，进行无常数项回归
// 由于常数项很显著，故忽略常数项导致估计偏差，得不到一致估计
reg lnw s expr tenure smsa rns,noc
// 只对南方居民的子样本进行回归，可使用虚拟变量rns
reg lnw s expr tenure smsa if rns
// 只对北方居民的子样本进行回归，~表示逻辑的否运算
reg lnw s expr tenure smsa if ~rns
// 只对中学以上(s>=12)的子样本进行回归
reg lnw s expr tenure smsa rns if s>=12
// 只对中学以上(s>=12)且在南方居住的子样本进行回归
reg lnw s expr tenure smsa if s>=12 & rns
// 回到最初估计的全样本，qui表示不汇报回归结果
qui reg lnw s expr tenure smsa rns
// 将被解释变量的拟合值记为lnw1
predict lnw1
// 将残差记为e
predict e,r
// 回归方程：lnw=beta_1+beta_2*s+beta_3*expr+beta_4*tenure+beta_5*smsa+beta_6*rns+epsilon
// 检验教育投资回报率是否为10%，即检验原假设H0: beta_2=0.1，原假设为变量s的系数为0.1
te s=0.1
/**
 * 由于t分布的平方为F分布，故Stata统一汇报F统计量及其p值。
 * 结果显示p值=0.6515，故无法拒绝原假设。
 */
// 手工计算t统计量：t=(估计值-假想值)/标准误=(0.102643-0.1)/0.0058488=0.45188757
// ttail表示自由度为752的t分布比0.45188757更大的右侧尾部概率
dis ttail(752, 0.45188757) * 2
// 如果进行单边检验，如原假设为H0: beta_2=0.1，而替代假设为H1: beta_2>0.1，则拒绝域在t分布右侧尾部
dis ttail(752, 0.45188757)
/**
 * 由于p值仍高达0.3257，故依然可以接受原假设。
 */

// 检验expr与tenure的系数是否相等，即检验H0: beta_3=beta_4
te expr=tenure
/**
 * 由于p值=0.8208，故可以接受原假设。
 */

// 检验工龄回报率与现单位年限回报率之和是否等于教育回报率，即H0: beta_3+beta_4=beta_2
te expr+tenure=s
/**
 * 由于p值=0.0031，故可在1%的显著性水平上拒绝原假设，即认为beta_3+beta4不等于beta_2。
 */

