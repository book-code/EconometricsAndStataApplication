* 4.7 一元回归的Stata实例
// 使用数据集grilic.dta
use grilic.dta,clear
// 将工资对数（lnw）对教育年限（s）进行一元回归
reg lnw s
/** 显示结果
 * Coef.表示回归系数(Coefficient)，_cons表示常数项(constant)。
 * SS列对应Total表示TSS，Model表示ESS，Residual表示RSS。
 * R-squared表示拟合优度，0.2525即教育年限约可解释工资对数25%的变动。
 */

// 无常数回归
reg lnw s,noc

* 4.8 Stata命令运行结果的存储与调用
use grilic.dta,clear
// r-类命令的运行结果存储在"r()"，可以通过"return list"来显示
sum s
// 存储的结果还包含未显示的"r(Var)"(方差)、"r(sum)"(求和)等
return list
// 变异系数=标准差/平均值
display r(sd)/r(mean)

// e-类命令的运行结果存储在"e()"，可以通过"ereturn list"来显示
reg lnw s
// 包括标量(scalars)、宏(macros)、系数矩阵(e(b))与协方差矩阵(e(V))，以及函数(functions)
ereturn list

* 4.9 总体回归函数与样本回归函数：蒙特卡罗模拟
// 删除内存中已有数据
clear
// 确定随机抽样的样本容量为30
set obs 30
// 指定随机抽样的种子为10101
set seed 10101
// 得到服从N(3,4)分布的随机样本，记为x
gen x = rnormal(3,4)
// 得到服从N(0,9)分布的随机样本，记为e
gen e = rnormal(0,9)
// 计算被解释变量y
gen y = 1 + 2 * x + e
// 把y对x进行OLS回归
reg y x
// 把总体回归函数(PRF)、散点图与样本回归函数画在一起
twoway function PRF = 1 + 2 * x,range(-5 15) || scatter y x || lfit y x,lpattern(dash)