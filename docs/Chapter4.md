# 4.一元线性回归

## 4.4 平方和分解公式

$$
\underbrace{\sum_{i=1}^{n}\left(y_{i}-\overline{y}\right)^{2}}_{\mathrm{TSS}}=\underbrace{\sum_{i=1}^{n}\left(\hat{y}_{i}-\overline{y}\right)^{2}}_{\mathrm{ESS}}+\underbrace{\sum_{i=1}^{n}e_{i}^{2}}_{\mathrm{RSS}}
$$

其中，$\bar{y}\equiv\frac1n\sum_{i\operatorname{=}1}^ny_i$为$y_i$的样本均值，$\hat{y}_{i}$为$y_i$的拟合值或预测值。

TSS(Total Sum of Squares)表示离差平方和，第一项ESS(Explained Sum of Squares)表示可由模型解释的部分，第二项RSS(Residual Sum of Squares)为残差平方和，是模型无法解释的部分。

## 4.5 拟合优度

**定义**：拟合优度(goodness of fit)$R^2$为

$$
R^2\equiv\frac{\sum_{i=1}^n\left(\hat{y}_i-\overline{y}\right)^2}{\sum_{i=1}^n\left(y_i-\overline{y}\right)^2}=1-\frac{\sum_{i=1}^ne_i^2}{\sum_{i=1}^n\left(y_i-\overline{y}\right)^2}
$$

在有常数项的情况下，拟合优度等于被解释变量$y_i$与拟合值$\hat{y}_{i}$之间相关系数的平方，即$R^2=[Corr(y_i,\hat{y}_{i})]^2$。

取值范围$0\leq R^2\leq 1$，$R^2$越高，则样本回归线对数据的拟合程度越好。

## 4.9 总体回归函数与样本回归函数：蒙特卡罗模拟

蒙特卡罗法(Monte Carlo Methods, MC)是指通过计算机模拟，从总体抽取大量随机样本的计算方法。

考虑如下总体回归模型：

$$
y_i=1+2x_i+\epsilon_i \quad(i=1,\cdots,30)
$$

其中，解释变量$x_i\sim N(3,2^2)$，扰动项$\epsilon_i\sim N(0,3^2)$，样本容量为$n=30$。我们随机抽取30个解释变量$x_i$的观测值和30个扰动项$\epsilon_i$的观测值，然后根据总体回归模型计算相应的被解释变量$y_i$。最后把$y_i$对$x_i$进行回归，得到样本回归函数(SRF)，并与总体回归函数(PRF)进行比较。
