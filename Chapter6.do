* 6.1 为何需要大样本理论
// 将数据集grilic.dta的工资与工资对数的核密度图画在一起
use grilic.dta,clear
gen wage=exp(lnw)
// xaxis(1) yaxis(1)与xaxis(2) yaxis(2)指定对于变量wage与lnw分别使用不同的x轴与y轴
// xvarlab(wage)与xvarlab(ln(wage))将变量wage与lnw核密度图的横轴标签分别指定为wage与ln(wage)
twoway kdensity wage,xaxis(1) yaxis(1) xvarlab(wage) || kdensity lnw,xaxis(2) yaxis(2) xvarlab(ln(wage)) lpattern(dash)

// 将教育年限s与其对数lns的核密度图画在一起
gen lns=log(s)
twoway kdensity s,xaxis(1) yaxis(1) xvarlab(s) || kdensity lns,xaxis(2) yaxis(2) xvarlab(lns) lpattern(dash)

* 6.2 随机收敛

// 画N(0,1)、t(1)与t(5)的累积分布函数
twoway function N=normal(x),range(-5 5) || function t1=t(1,x),range(-5 5) lp(dash) || function t5=t(5,x) lp(shortdash) ytitle(累积分布函数)
// 概率密度函数
twoway function N=normalden(x),range(-5 5) || function t1=tden(1,x),range(-5 5) lp(dash) || function t5=tden(5,x) lp(shortdash) ytitle(概率密度)

* 6.4 使用蒙特卡罗法模拟中心极限定理
// 定义程序onsample，并以r()形式存储结果
program onesample,rclass
  // 删去内存中已有数据
  drop _all
  // 确定随机抽样的样本容量为30
  set obs 30
  // 得到在(0,1)上均匀分布的随机样本
  gen x=runiform()
  // 可以换成从卡方分布中抽取随机样本
  //gen x=rchi2(10)
  // 使用命令sum计算样本均值
  sum x
  // 将样本均值记为mean_sample
  return scalar mean_sample=r(mean)
  // 程序onesample结束
  end
// 指定Stata输出结果连续翻页
set more off
// repos(10000)表示命令simulate将运行onesample程序10000遍，并生成变量xbar来记录这10000个样本均值
// seed(101)用来固定随机数种子
// nodots指不显示表示模拟过程的点
simulate xbar=r(mean_sample),seed(101) repos(10000) nodots:onesample
// 画样本均值的直方图，normal表示画出相应的正态分布
hist xbar,normal

* 6.10 大样本OLS的Stata实例
use nerlove.dta,clear
// 使用普通标准误进行OLS估计
reg lntc lnq lnpl lnpk lnpf

// 由于lnq的系数为1/r(即规模报酬的倒数)，故可估计规模报酬为
// _b[lnq]表示lnq的OLS系数估计值
di 1/_b[lnq]

// 检验规模报酬不变的原假设H0:r=1
te lnq=1

// 在Stata中使用稳健标准误，即可进行大样本检验
// 使用稳健标准误重新进行回归
reg lntc lnq lnpl lnpk lnpf,robust
// 检验变量lnq的系数是否为1
te lnq=1

* 6.11 大样本理论的蒙特卡洛模拟
// 定义程序chi2data，以r()形式存储结果
program chi2data,rclass
  drop _all
  // 将样本容量改为100和1000再查看结果
  set obs 20
  gen x=rchi2(1)
  gen y=1+2*x+rchi2(10)-10
  reg y x
  return scalar b=_b[x]
  end
set more off
// repos(10000)表示通过命令simulate将程序chi2data模拟10000次
simulate bhat=r(b),repos(10000) seed(10101) nodots:chi2data 
// 计算10000个bhat的均值与标准差
sum bhat
// 通过直方图来看这10000个bhat的分布
hist bhat,normal