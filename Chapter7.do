* 7.5 处理异方差的Stata命令及实例
// 打开数据集nerlove.dta
use nerlove.dta,clear
// 以OLS估计对数形式的成本函数
reg lntc lnq lnpl lnpk lnpf

* 画残差图
// 为初步考察是否存在异方差，下面画残差与拟合值的散点图
rvfplot
/**
 * 结果显示当lntc的拟合值较小时，扰动项的方差较大
 */
 
// 进一步考察残差与解释变量lnq的散点图
rvpplot lnq
/**
 * 结果显示lnq越小，扰动项的方差越大
 * 以上两张图说明很可能存在异方差，即扰动项的方差随着观测值而变
 */

* BP检验
// quietly表示不在Stata结果窗口显示运行结果
quietly reg lntc lnq lnpl lnpk lnpf
// estat指在完成估计后所计算的后续统计量
// hettest表示heteroskedasticity test
// iid表示仅假定数据为iid，而无需正态假定
// rhs表示使用全部解释变量进行辅助回归，默认使用拟合值进行辅助回归
// 使用拟合值进行BP检验
estat hettest,iid
// 使用所有解释变量进行BP检验
estat hettest,iid rhs
// 使用变量lnq进行BP检验
estat hettest lnq,iid
/**
 * 以上各种BP检验的p值都等于0，故强烈拒绝同方差的原假设，认为存在异方差
 */

* 怀特检验
// imtest指信息矩阵检验(information matrix test)
estat imtest,white
/**
 * 结果显示p值等于0，故拒绝同方差的原假设，认为存在异方差
 */

* WLS
quietly reg lntc lnq lnpl lnpk lnpf
// 首先计算残差，并记为e1
predict e1,residual
// 其次生成残差的平方，并记为e2
gen e2=e1^2
// 将残差平方取对数
gen lne2=log(e2)
// 假设lne2为变量lnq的线性函数，进行以下辅助回归
reg lne2 lnq
/**
 * 尽管lnq在1%水平上显著，但R^2仅为0.1309，而且常数项不显著
 */

// 去掉常数项，重新进行辅助回归
reg lne2 lnq,noc
/**
 * R^2上升为0.7447，残差平方的变动与lnq高度相关
 */

// 计算以上辅助回归的拟合值，并记为lne2f
predict lne2f
// 去掉对数后，即得到方差的估计值，并记为e2f
gen e2f=exp(lne2f)
// 最后使用方差估计值的倒数作为权重，进行WLS回归
// aw表示analytical weight，为扰动项方差的倒数
reg lntc lnq lnpl lnpk lnpf [aw=1/e2f]
// 使用稳健标准误进行WLS估计
reg lntc lnq lnpl lnpk lnpf [aw=1/e2f],r
